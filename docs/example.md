Example usage:

```
mikrotik_ntp:
  enabled: yes # Sets NTP client to enabled/disabled state
  primary-ntp: 0.0.0.0 # Primary NTP server IP. Could be set to 0.0.0.0 or omitted if you prefer using DNS names
  secondary-ntp: 0.0.0.0 # Secondary NTP server IP.  Could be set to 0.0.0.0 or omitted if you prefer using DNS names
  server-dns-names: # List of DNS names of NTP servers. Required configured DNS client on Mikrotik RouterOS
    - 0.pool.ntp.org
    - 1.pool.ntp.org
    - 2.pool.ntp.org
```
